function turbolinksHover() {
	var self = this;

	self.hoverTime = 400;
	self.fetchers = {};
	self.doc = document.implementation.createHTMLDocument('prefetch');
}

turbolinksHover.prototype.start = function() {
	var self = this;

	// Listening to hover event
	document.addEventListener('mouseover', function(event) {
		// If it's not a link
		if (!event.target.href) return;
		// Skipping the link if it's not on the current site
		if(!event.target.href.indexOf('https://' + window.location.host) == -1 && !event.target.href.indexOf('http://' + window.location.host) == -1 && !event.target.href.indexOf('/') != 0) {
			return;
		}
		// If the link is meant to be ignored
		if(event.target.hasAttribute('data-turbolinks') && !event.target.getAttribute('data-turbolinks')) {
			return;
		}
		// If the links is meant to be ignored because of a parent element
		if(event.target.closest('[data-turbolinks]')) {
			return;
		}

		// Get the url
		var url = event.target.href;
		// If already prefetched or prefetching quit
		if (self.prefetched(url) || self.prefetching(url)) {
			return;
		}
		// Cleanup the events
		self.cleanup(event);
		event.target.addEventListener('mouseleave', self.cleanup.bind(self));
		self.fetchers[url] = setTimeout(function(){
			self.prefetch(url);
		}, self.hoverTime);
	});	
}
	
turbolinksHover.prototype.fetchPage = function(url, success) {
  var xhr = new XMLHttpRequest();
  var self = this;

  xhr.open('GET', url)
  xhr.setRequestHeader('VND.PREFETCH', 'true')
  xhr.onreadystatechange = function(){
    if (xhr.readyState !== XMLHttpRequest.DONE) return;
    if (xhr.status !== 200) return;
    success(xhr.responseText);
  }
  xhr.send();
}

turbolinksHover.prototype.prefetchTurbolink = function(url) {
	var self = this;

	self.fetchPage(url, function(responseText) {
		self.doc.open();
		self.doc.write(responseText);
		self.doc.close();
		var snapshot = Turbolinks.Snapshot.fromElement(self.doc.documentElement);
		Turbolinks.controller.cache.put(url, snapshot);
	})
}

turbolinksHover.prototype.prefetch = function(url) {
	var self = this;

	if (self.prefetched(url)) return;
	self.prefetchTurbolink(url);
}

turbolinksHover.prototype.prefetched = function(url) {
	var self = this;

  	return location.href === url || Turbolinks.controller.cache.has(url)
}

turbolinksHover.prototype.prefetching = function(url) {
	var self = this;

  	return !!self.fetchers[url]
}

turbolinksHover.prototype.cleanup = function(event) {
	var self = this;

	var element = event.target;
	clearTimeout(self.fetchers[element.href]);
	element.removeEventListener('mouseleave', self.cleanup);
}

if(typeof TurbolinksHover === 'undefined') {
	var TurbolinksHover = new turbolinksHover();
}