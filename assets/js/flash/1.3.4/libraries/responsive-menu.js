/**
 * Responsive Menu System
 * ----------------------
 */

var flashResponsiveMenu = function(menu) {
	var self = this;

	self.active_class = 'responsive_menu--in';
	self.menu = document.body.querySelector(menu);
	self.menu_name = self.menu.getAttribute('data-flash-responsive-menu');
	self.transitioning = false;

	self.init();

	return self.menu;
}


/*
/*
		$('body').css({
			position: 'fixed',
			top: scroll_top + 'px'
		});

		var scroll_top = $('body').css('top');
		$('body').css({
			position: 'relative',
			top: 0
		});
 */

/**
 * Init the menu and bind events
 * @return void
 */
flashResponsiveMenu.prototype.init = function() {
	var self = this;

	// Check if the menu is already initialised on this element 
	// and if the menu has a name set
	if(self.menu.classList.contains('js_init--responsive_menu') || !self.menu_name) {
		return false;
	} else {
		self.menu.classList.add('js_init--responsive_menu');
	}

	// Get the trigger
	self.trigger = document.querySelector('[data-flash-responsive-menu-trigger="' + self.menu_name + '"]');

	// Open the menu clicking on the trigger
	self.trigger.addEventListener('click', function(e) {
		e.preventDefault();
		self.open();
	}.bind(this), false);

	// Close the menu clicking on the menu
	self.menu.addEventListener('click', function(e){
		if(!e.target.closest('.responsive_navigation') && self.isActive()) {
			self.close();
		}
	}.bind(this), false);
	
	// Close the menu clicking on the close icon
	self.menu.querySelector('[data-close-responsive-menu]').addEventListener('click', function(e){
		e.preventDefault();
		self.close();
	}.bind(this), false);

	// Open a submenu when the page is loaded
	var clicked_triggers = [];
	document.body.querySelectorAll('.responsive_navigation [data-dropdown] a').forEach(function(anchor){
		var locations = [];

		if(anchor.hasAttribute('href') && window.location.pathname.indexOf(anchor.getAttribute('href')) > -1) {
			var parent_dropdown = anchor.closest('[data-dropdown]');
			var dropdown_name = parent_dropdown.getAttribute('data-dropdown');
			var trigger = document.body.querySelector('[data-dropdown-target="' + dropdown_name + '"]');
			if(clicked_triggers.indexOf(trigger.innerText) == -1) {
				parent_dropdown.style.maxHeight = '';
				parent_dropdown.style.display = 'block';
				trigger.classList.add('open');
				clicked_triggers.push(trigger.innerText);
			}
		}
	});

	/**
	 * SUBMENU STUFF
	 */
	flash.listen(self.menu.querySelectorAll('[data-submenu-target]'), 'click', function(e){
		if(self.transitioning) {
			return;
		}
		self.transitioning = true;
		var clicked_link = e.target;
		var menu = clicked_link.closest('.responsive_navigation__menu');
		if(!clicked_link.dataset.submenuTarget) {
			return;
		}

		var submenu = self.menu.querySelector('[data-submenu="' + clicked_link.dataset.submenuTarget + '"]');
		if(!submenu) {
			return;
		}

		// ANIMATING THE MENUS
		// class visible makes the items visible
		// class next is applied to the next menu that will appear from the right
		// move_next, combined with next will make the menu come from the right
		// move_next, not combined with next will make the current menu move out to the left
		// the same idea is applied to the move_prev and prev classes below
		submenu.classList.add('visible');
		submenu.classList.add('next');

		function moveSubmenuNext() {
			self.transitioning = false;
			submenu.classList.remove('next');
			submenu.classList.remove('move_next');
			menu.classList.remove('visible');
			menu.classList.remove('move_next');
			// Unbind event so we don't mess up all the things
			submenu.removeEventListener('transitionend', moveSubmenuNext);
		}

		setTimeout(function(){
			document.querySelector('.responsive_navigation__menus').scrollTop = 0;
			submenu.classList.add('move_next');
			menu.classList.add('move_next');
			// Cleaning up classes when the move transition is over
			submenu.addEventListener('transitionend', moveSubmenuNext);
		}, 80);

		// Binding event to go back (read the info about the "next" animation because criteria is the same)
		// in case a back link is defined
		// back link is defined adding the data-submenu-back attribute to the element you want to use
		// for this purpose
		var back_link = submenu.querySelector('[data-submenu-back]');
		if(back_link) {
			function transitionBack() {
				if(self.transitioning) {
					moveSubmenuNext();
				}
				self.transitioning = true;
				// Animating the menus
				menu.classList.add('visible');
				menu.classList.add('prev');

				setTimeout(function(){
					submenu.classList.add('move_prev');
					menu.classList.add('move_prev');
					menu.addEventListener('transitionend', moveSubmenuPrev);

					function moveSubmenuPrev() {
						self.transitioning = false;
						submenu.classList.remove('visible');
						submenu.classList.remove('move_prev');
						menu.classList.remove('prev');
						menu.classList.remove('move_prev');
						// Unbind event so we don't mess up all the things
						menu.removeEventListener('transitionend', moveSubmenuPrev);
					}
				}, 80);		
				back_link.removeEventListener('click', transitionBack);
			}

			back_link.addEventListener('click', transitionBack);
		}
	});
}

/**
 * Check if this menu is open
 * @return {Boolean} true if the menu is active
 */
flashResponsiveMenu.prototype.isActive = function() {
	var self = this;

	return self.menu.classList.contains(self.active_class)
}

/**
 * Open the menu
 * @return {void}
 */
flashResponsiveMenu.prototype.open = function() {
	var self = this;

	if(self.isActive()) {
		return;
	}

	self.menu.classList.add('responsive_menu--in');
	document.body.classList.add('overlay');

	// No scroll allowed here
	document.body.style.overflowY = 'hidden';
	document.body.style.height = window.offsetHeight;
	document.body.style.top = window.scrollY * -1 + 'px';
	document.body.style.position = 'fixed';
	self.transitioning = true;
	self.menu.querySelector('.responsive_navigation__menu:not([data-submenu])').classList.add('visible');

	setTimeout(function(){
		self.active = true;
		self.transitioning = false;
	}, 200);
}

/**
 * Close the menu
 * @return {void}
 */
flashResponsiveMenu.prototype.close = function() {
	var self = this;

	if(!self.isActive()) return;

	document.body.style.overflowY = 'auto';
	document.body.style.height = 'auto';
	document.body.style.position = 'relative';
	window.scroll(0, parseInt(document.body.style.top) * -1);
	document.body.style.top = '0';

	self.menu.classList.remove('responsive_menu--in');
	document.body.classList.remove('overlay');

	self.active = false;
	setTimeout(function(){
		self.menu.querySelectorAll('.responsive_navigation__menu').forEach(function(menu){
			menu.classList.remove('visible');
		});
	}, 400);
}
