flashCore.prototype.slideUp = function(el, timing, callback) {
    var self = this;

    if(!timing) {
        timing = 500;
    }
    var animation_timing_ms = timing;

    var transitions = el.style['transition'];

    el.style.maxHeight             = el.offsetHeight + 'px';
    el.style['transition']         = 'max-height ' + animation_timing_ms + 'ms ease-in-out';
    el.style.overflowY             = 'hidden';  
    setTimeout(function(){
        el.style.maxHeight         = '0';
    }, 1);

    setTimeout(function(){
        el.style.transitions = transitions;
        if(callback) {
            callback();
        }
    }, timing + 1);
}